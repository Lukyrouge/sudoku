module.exports = class Sudoku {
    constructor(width, socket) {
        this.table = new Array(width * width);
        this.possible = new Array(width * width);
        for (let i = 0; i < width * width; i++) {
            this.table[i] = 0;
            this.possible[i] = [];
            for (let j = 1; j <= width; j++) this.possible[i].push(j);
        }
        this.n = width;
        this.sn = Math.sqrt(width);
        this.socket = socket;
    }

    getLog(pos) {
        console.log("Position: " + pos, "Possible: " + this.possible[pos], "val: " + this.table[pos], "availables: " + this.availableNumbers(pos));
    }

    generateCell(pos) {
        setTimeout(() => {
            let nums = this.availableNumbers(pos, false);
            if (nums.length === 0) {
                // No available numbers --> backtrack.
                if (this.possible[pos].length === 0) this._genPossible(pos);
                // console.log(pos, nums);
                // console.log("BACKTRACK", pos);
                if (pos !== 0) {
                    this._removePrevPossible(pos);
                    this.generateCell(pos - 1);
                }
            } else {
                this.table[pos] = nums[Math.floor(Math.random() * nums.length)];
                this.generateCell(pos + 1);
            }
            this.socket.emit('cell', {i: pos, n: this.table[pos]});
        }, 100);
    }

    availableNumbers(pos) {
        let possible = this.possible[pos], nums = [];
        for (let num = 1; num <= this.n; num++) {
            if (possible.includes(num) && this.availableNumber(num, pos)) {
                nums.push(num);
            }
        }
        return nums;
    }

    availableNumber(num, pos) {
        return this._unUsedInRow(num, pos) && this._unUsedInCol(num, pos) && this._unUsedInBox(num, pos);
    }

    _unUsedInRow(num, pos) {
        let row = Math.floor(pos / this.n);
        for (let c = 0; c < this.n; c++) {
            let p = this.n * row + c;
            if (this.table[p] === num) return false;
        }
        return true;
    }

    _unUsedInCol(num, pos) {
        let col = pos % this.n;
        for (let r = 0; r < this.n; r++) {
            let p = this.n * r + col;
            if (this.table[p] === num) return false;
        }
        return true;
    }

    _unUsedInBox(num, pos) {
        let col = pos % this.n, row = Math.floor(pos / this.n);
        let pc = col - col % this.sn, pr = row - row % this.sn;
        for (let c = 0; c < this.sn; c++) {
            for (let r = 0; r < this.sn; r++) {
                let p = this.n * (pr + r) + (pc + c);
                if (this.table[p] === num) return false;
            }
        }
        return true;
    }

    _removePrevPossible(pos) {
        pos -= 1;
        let val = this.table[pos];
        this.possible[pos] = this.possible[pos].filter(n => n !== val);
        this.table[pos] = 0;
    }

    _genPossible(pos) {
        let p = [];
        for (let i = 1; i <= this.n; i++) p.push(i);
        this.possible[pos] = p;
    }
};