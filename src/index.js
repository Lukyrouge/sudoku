const express = require('express');
const app = express();

const io = require('socket.io').listen(app.listen(3000));
const Sudoku = require('./Sudoku.js');

app.get('/', async function (req, res) {
    res.render("home.pug");
});

app.use(express.static('public'));
app.use(express.urlencoded({extended: true}));

io.sockets.on('connection', (socket) => {
    socket.sudoku = new Sudoku(9, socket);
    socket.sudoku.generateCell(0);
    console.log('connection');

    socket.on('click', id => {
        socket.sudoku.getLog(id);
    })
});
