$('document').ready(() => {
    let socket = io('http://localhost:3000');

    socket.on('table', table => {
        $('#table').empty();
        for (let i = 0; i < 9 * 9; i++) {
            if (i % 9 === 0) $('#table').append('<tr id=t' + Math.floor(i / 9) + '></tr>');
            let v = table[Math.floor(i / 9)][i % 9];
            if (v === 0) $('#t' + Math.floor(i / 9)).append('<td></td>');
            else $('#t' + Math.floor(i / 9)).append('<td>' + v + '</td>');
        }
    });


    initTable();

    socket.on('cell', data => {
        console.log(data);
        $('#' + data.i).empty();
        if (data.n !== 0)  $('#' + data.i).append(data.n);
        $('.green').removeClass('green');
        $('#' + data.i).addClass('green');
    });

    function initTable() {
        $('#table').empty();
        for (let i = 0; i < 9 * 9; i++) {
            if (i % 9 === 0) $('#table').append('<tr id=t' + Math.floor(i / 9) + '></tr>');
            $('#t' + Math.floor(i / 9)).append('<td id="' + i + '"></td>');
        }
    }

    socket.on('solved', table => {
        $('#solved').empty();
        for (let i = 0; i < 9 * 9; i++) {
            if (i % 9 === 0) $('#solved').append('<tr id=s' + Math.floor(i / 9) + '></tr>');
            let v = table[Math.floor(i / 9)][i % 9];
            if (v === 0) $('#s' + Math.floor(i / 9)).append('<td id="' + i + '"></td>');
            else $('#s' + Math.floor(i / 9)).append('<td id="' + i + '">' + v + '</td>');
        }
    });

    $('body').on('click', 'td', function () {
        // alert('test');
        socket.emit('click', $(this)[0].id);
    });
});